VERSION=0.1.2.6

.PHONY: warn install build
warn:
	@echo "Use \"make install\" to download and compile dependencies and install htalkat"
install: *.hs
	cabal update && cabal install
build: *.hs
	cabal build

htalkat.1: htalkat.1.md
	pandoc --standalone -f markdown -t man < htalkat.1.md | sed 's/\$$VERSION/${VERSION}/g' >| htalkat.1

dist-newstyle/sdist/htalkat-${VERSION}.tar.gz: *.hs README.md CHANGELOG.md COPYING *.cabal htalkat.1
	cabal sdist

htalkat-${VERSION}-src.tgz: dist-newstyle/sdist/htalkat-${VERSION}.tar.gz
	cp $< $@

htalkat.bundle: .git/refs/heads/master
	git bundle create "$@" HEAD master

index.gmi: index.gmi.in Makefile
	cat $< | sed 's/\$$VERSION/${VERSION}/g' > $@

index.html: index.gmi
	cat $< | sed s/\.gmi/.html/g | ./tools/gmi2html.sed > $@

index.md: index.gmi
	cat $< | sed s/\.gmi/.md/g | ./tools/gmi2md.sed > $@

%.md: %.gmi
	./tools/gmi2md.sed < $< > $@

%.html: %.gmi
	./tools/gmi2html.sed < $< > $@

publish: htalkat-${VERSION}-src.tgz htalkat.bundle index.gmi index.html README.md README.gmi README.html CHANGELOG.gmi CHANGELOG.md CHANGELOG.html spec.gmi spec.html
	cp $^ /var/gemini/gemini.thegonz.net/htalkat/
	scp $^ sverige:html/htalkat/
