-- This file is part of htalkat
-- Copyright (C) 2021 Martin Bays <mbays@sdf.org>
--
-- This program is free software: you can redistribute it and/or modify
-- it under the terms of version 3 of the GNU General Public License as
-- published by the Free Software Foundation, or any later version.
--
-- You should have received a copy of the GNU General Public License
-- along with this program.  If not, see http://www.gnu.org/licenses/.

module Command where

import           Data.Char (toLower)
import           Data.List (isPrefixOf)
import           Safe      (headMay)

data Command
    = Help
    | Identity
    | Name
    | Answer
    | Connect
    | Listen
    deriving (Eq,Ord,Show,Enum)

commands :: [Command]
commands = enumFrom Help

cmdOfStr :: String -> Maybe Command
cmdOfStr s = headMay [ c
    | c <- commands
    , s `isPrefixOf` (toLower <$> show c) ]
