# htalkat Changelog

## 0.1.2
* Fix compatibility problems with older ghc; any base >= 4.9 should now work.
* Fix linebreaks in dumb client.
* Add per-command options mirroring most config options.
* Add options to use dumb client in place of curses client.

## 0.1.1
* First release
