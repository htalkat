% HTALKAT(1) Version $VERSION | htalkat
# NAME
htalkat - talkat client and server

# SYNOPSIS
talkat [OPTION]... COMMAND [ARG]...

# DESCRIPTION
htalkat is a client and server for the talkat real-time text protocol, with a
curses interactive client. Similar to talk(1) but running over a TLS
connection.

# COMMANDS
h[elp] [COMMAND]
: Give detailed usage information.

i[dentity] [PUBLIC\_NAME]
: Create/show/modify cryptographic identity (public and secret keys, and
: certificates), setting public name to PUBLIC\_NAME if given.

c[onnect] ( [talkat:]FP@HOST | NAME[@HOST] )
: Connect to remote host, checking the public key agrees with the
: fingerprint provided. Waits for the connection to be "answered", then starts
: the interactive curses client (or a user-configured client)

n[ame] ( [talkat:]FP@HOST | NAME[@HOST] ) [NAME]
: Set short name ("petname") for fingerprint and optional host. This can be
: used with the c[onnect] command, and will be used when referring to the
: fingerprint. If the name to set is omitted, it will be prompted for.

l[isten]
: Start server process. This will listen for connections on the configured
: port (default: 5518), and notify by writing information to stdout and
: executing the ~/.htalkat/notify.sh script. If the public key provided on an
: incoming connection does not have a name associated, it will be assigned an
: automatic name +N where N is the next available positive integer.

a[nswer] [\-\-list|-l] [NAME]
: Answer incoming connection, starting the interactive curses client (or
: a user-configured client), or list pending unanswered connections.

a[nswer] [\-\-interactive-client|-i] NAME SOCKET\_PATH
: Directly spawn default (curses) client. NAME is the name of remote to
: display. SOCKET\_PATH is the path to a unix domain socket to interact with;
: characters read from this socket are displayed as coming from remote,
: and user input is written to the socket.

# OPTIONS
-d, \-\-datadir *PATH*
: Directory for data and configuration.
: Default: ~/.htalkat

-S, \-\-socks-host *HOST*
: Use SOCKS5 proxy for all connections (including DNS resolution).

-P, \-\-socks-port *PORT*
: Port to use for SOCKS5 proxy.
: Default: 1080.

-v, \-\-version
: Show version information.

-h, \-\-help
: Show usage information.

# FILES
~/.htalkat
: Default *datadir*. The paths given below assume that this is used.

~/.htalkat/id.key
: Secret key used in certificates. The user hash is derived from the
: corresponding public key.

~/.htalkat/id-connect.crt
: X509 certificate used for outgoing connections.

~/.htalkat/id-listen.crt
: X509 certificate used for incoming connections.
: Unlike id-connect.crt, this will typically have empty Common Name field,
: for privacy reasons.

~/.htalkat/htalkat.conf
: Configuration file.

~/.htalkat/notify.sh
: Executable script which is run when the server accepts an incoming
: connection.

~/.htalkat/names/
: Short names ("petnames") for user hashes, optionally with hosts, are stored
: here. These files may be manipulated directly.

~/.htalkat/incoming/
: Information on unanswered incoming connections is stored here.

~/.htalkat/logs/
: Communication logs are optionally written here, one file per connection.

# ENVIRONMENT
HTALKAT\_DIR
: Path to directory to use in place of ~/.htalkat/ (can be overridden by -d).

# LICENCE
htalkat is free software, released under the terms of the GNU GPL v3 or later.
You should have obtained a copy of the licence as the file COPYING.

# AUTHORS
Martin Bays <mbays@sdf.org>
