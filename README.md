# htalkat: Haskell client and server for talkat (Talk Across TLS)

# Building from source
Install the haskell package manager cabal, and the ncurses library and headers;
e.g. on a debian system:
```
$ sudo apt-get install cabal-install libncursesw5-dev
```
Then in the htalkat directory, run:
```
$ cabal update && cabal install
```
The resulting binary will be installed by default as `~/.cabal/bin/htalkat`.

## Compile-time options
* `cabal install -f -curses`: compile without curses; a very simple dumb client is used instead.

# Basic usage
```
# Create your cryptographic identity:
htalkat i
# Connect to a talkat URI:
htalkat c talkat:1fb3034c92e2b5d32f78739995abf31b@example.invalid
# Give it a short name:
htalkat n talkat:1fb3034c92e2b5d32f78739995abf31b@example.invalid bob
htalkat c bob

# Host your own talkat server; by default this will run on port 5518.
htalkat l
# Wait for someone to connect, then answer:
htalkat a

# Get further help
htalkat h
```

# Configuration
After first run, you can edit ~/.htalkat/htalkat.conf for general configuration options, and ~/.htalkat/notify.sh to set how the server notifies you of incoming connections.

# Portability
htalkat was written with POSIX systems in mind, and has only been tested on them, but it may also be possible to compile and run it on Windows. Please let me know if you try.

-- mbays@sdf.org
