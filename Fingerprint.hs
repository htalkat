-- This file is part of htalkat
-- Copyright (C) 2021 Martin Bays <mbays@sdf.org>
--
-- This program is free software: you can redistribute it and/or modify
-- it under the terms of version 3 of the GNU General Public License as
-- published by the Free Software Foundation, or any later version.
--
-- You should have received a copy of the GNU General Public License
-- along with this program.  If not, see http://www.gnu.org/licenses/.

{-# LANGUAGE Safe #-}

module Fingerprint where

import           Control.Monad   (guard)

import qualified Data.ByteString as BS

import           HexString

-- | 16-byte SHA256/128 fingerprint
newtype Fingerprint = Fingerprint BS.ByteString
    deriving (Eq,Ord,Show)

parseFingerprint :: String -> Maybe Fingerprint
parseFingerprint s = do
    guard $ length s == 32
    Fingerprint <$> parseHexString s

showFingerprint :: Fingerprint -> String
showFingerprint (Fingerprint fp) = showHexString fp
